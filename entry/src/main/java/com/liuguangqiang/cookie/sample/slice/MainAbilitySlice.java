/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.liuguangqiang.cookie.sample.slice;

import com.liuguangqiang.cookie.CookieBar;
import com.liuguangqiang.cookie.OnActionClickListener;
import com.liuguangqiang.cookie.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.StackLayout;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbilitySlice extends AbilitySlice {
    /*
    *
1.CookieBar高度获取
2.开始显示闪
3.点击事件
4.文字显示问题
    *
    * */

    private String mTitle = "这是一个标题";
    private String mContent = "咦？你又变帅了！！！咦？你又变帅了！！！咦？你又变帅了！！！咦？你又变帅了！！！";
    private String mAction = "点击一下";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        StackLayout decorView = (StackLayout) findComponentById(ResourceTable.Id_activity_main);

        findComponentById(ResourceTable.Id_btn_top).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new CookieBar.Builder(decorView)
                        .setTitle(mTitle)
                        .setMessage(mContent)
                        .show();
            }
        });
        findComponentById(ResourceTable.Id_btn_bottom).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new CookieBar
                        .Builder(decorView)
                        .setTitle(mTitle)
                        .setIcon(ResourceTable.Media_icon)
                        .setMessage(mContent)
                        .setLayoutGravity(LayoutAlignment.BOTTOM)
                        .setAction(mAction, new OnActionClickListener() {
                            @Override
                            public void onClick() {
                                ToastDialog toastDialog = new ToastDialog(getContext());
                                toastDialog.setText("点击后，我更帅了!").show();
                            }
                        })
                        .show();
            }
        });
        findComponentById(ResourceTable.Id_btn_top_with_icon).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new CookieBar.Builder(decorView)
                        .setMessage(mContent)
                        .setDuration(10000)
                        .setActionWithIcon(ResourceTable.Media_close, new OnActionClickListener() {
                            @Override
                            public void onClick() {
                                ToastDialog toastDialog = new ToastDialog(getContext());
                                toastDialog.setText("点击后，我更帅了!").show();
                            }
                        })
                        .show();
            }
        });
        findComponentById(ResourceTable.Id_btn_custom).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new CookieBar.Builder(decorView)
                        .setTitle(mTitle)
                        .setMessage(mContent)
                        .setDuration(3000)
                        .setBackgroundColor(Color.BLUE.getValue())
                        .setActionColor(Color.WHITE.getValue())
                        .setTitleColor(Color.RED.getValue())
                        .setAction(mAction, new OnActionClickListener() {
                            @Override
                            public void onClick() {
                                ToastDialog toastDialog = new ToastDialog(getContext());
                                toastDialog.setText("点击后，我更帅了!").show();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
