Cookie [![Build Status](https://travis-ci.org/liuguangqiang/SwipeBack.png?branch=master)](https://travis-ci.org/liuguangqiang/CookieBar)
==============================================
CookieBar是一个轻量级的库，用于在屏幕顶部或底部显示简短的消息。
## 快照
![image](gif/cookiebar.gif)

## 集成

### 方案一

```
添加har包到lib文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

### 方案二

```
implementation project(':library')
```

### 方案三

```
implementation 'io.openharmony.tpc.thirdlib:CookieBar:1.0.2'
```

## 简单使用CookieBar.
```
 new CookieBar.Builder(MainActivity.this)
                        .setTitle("TITLE")
                        .setMessage("MESSAGE")
                        .show();
```

## 带图标和按钮的CookieBar使用
```
 new CookieBar.Builder(MainActivity.this)
                        .setTitle("TITLE")
                        .setIcon(R.mipmap.ic_launcher)
                        .setMessage("MESSAGE")
                        .setAction("ACTION", new OnActionClickListener() {
                            @Override
                            public void onClick() {
                            }
                        })
                        .show();
```

## 您可以通过设置主题的属性来更改默认样式

```
{
  "color":[
    {
      "name":"default_bg_color",
      "value":"#BDBDBD"
    },
    {
      "name":"default_title_color",
      "value":"#ffffff"
    },
    {
      "name":"default_message_color",
      "value":"#ffffff"
    },
    {
      "name":"white",
      "value":"#ffffff"
    },
    {
      "name":"default_action_color",
      "value":"#26a69a"
    }
  ]
}
```

## 或者使用cookie生成器动态更改样式。
 * layoutGravity
 * backgroundColor
 * titleColor
 * messageColor
 * actionColor
 * duration

## New Features
#### 1.0.0
 * 操作按钮可以替换为图标。

## License

    Copyright 2017 Eric Liu

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
