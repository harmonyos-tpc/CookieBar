package com.liuguangqiang.cookie;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * CookieBar is a lightweight library for showing a brief message at the top or bottom of the
 * screen. <p>
 * <pre>
 * new CookieBar
 *      .Builder(MainActivity.this)
 *      .setTitle("TITLE")
 *      .setMessage("MESSAGE")
 *      .setAction("ACTION", new OnActionClickListener() {})
 *      .show();
 * </pre>
 * <p> Created by Eric on 2017/3/2.
 */
public class CookieBar {

    private static final String TAG = "cookie";

    private Cookie cookieView;
    private ComponentContainer decorView;
    private Context context;
    private int widthScreen;
    private int heightScreen;
    private int heightCookie;

    private CookieBar() {
    }

    private CookieBar(ComponentContainer decorView, Params params) {
        this.decorView = decorView;
        this.context = decorView.getContext();
        cookieView = new Cookie(context);
        widthScreen = ScreenUtils.getScreenWidth(context);
        heightScreen = ScreenUtils.getScreenHeight(context);
//        heightCookie = cookieView.getHeight();
        heightCookie = 182 * 2;

        cookieView.setParams(params);
    }

    public void show() {
        if (cookieView != null) {

            if (cookieView.getComponentParent() == null) {
                decorView.addComponent(cookieView);
            }
            createInAnim();
            createOutAnim();

            cookieView.setLayoutRefreshedListener(new Component.LayoutRefreshedListener() {
                @Override
                public void onRefreshed(Component component) {
                    if (cookieView.getLayoutGravity() == LayoutAlignment.TOP) {
                        cookieView.setComponentPosition(0, 0, widthScreen, 2 * heightCookie);
                    } else {
                        cookieView.setComponentPosition(0, heightScreen - 2 * heightCookie, widthScreen, heightScreen);
                    }
                }
            });
        }
    }

    private AnimatorProperty slideInAnimation;
    private AnimatorProperty slideOutAnimation;


    private void createInAnim() {
        if (cookieView.getLayoutGravity() == LayoutAlignment.BOTTOM) {
            slideInAnimation = cookieView.createAnimatorProperty().moveFromY(heightScreen).moveToY(heightScreen - heightCookie);
        } else {
            slideInAnimation = cookieView.createAnimatorProperty().moveFromY(-heightCookie).moveToY(0);
        }
        slideInAnimation.setTarget(cookieView);
        slideInAnimation.setDuration(500);
        slideInAnimation.setCurveType(Animator.CurveType.ANTICIPATE_OVERSHOOT);
        slideInAnimation.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
//                cookieView.setVisibility(Component.VERTICAL);
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
                    @Override
                    public void run() {
                        dismiss();
                    }
                }, cookieView.getDuration());
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        slideInAnimation.start();
    }

    private void createOutAnim() {
        if (cookieView.getLayoutGravity() == LayoutAlignment.BOTTOM) {
            slideOutAnimation = cookieView.createAnimatorProperty().moveFromY(heightScreen - heightCookie).moveToY(heightScreen);
        } else {
            slideOutAnimation = cookieView.createAnimatorProperty().moveFromY(0).moveToY(-heightCookie);
        }
        slideOutAnimation.setDuration(500);
        slideOutAnimation.setCurveType(Animator.CurveType.ANTICIPATE_OVERSHOOT);
    }

    private void dismiss() {
        slideOutAnimation.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                cookieView.destroy();
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        slideOutAnimation.start();
    }

    public static class Builder {

        private Params params = new Params();

        private ComponentContainer decorView;
        private Context context;

        /**
         * Create a builder for an cookie.
         */
        public Builder(ComponentContainer decorView) {
            this.decorView = decorView;
            this.context = decorView.getContext();
        }

        /**
         * setIcon
         *
         * @param iconResId int
         * @return Builder Builder
         */
        public Builder setIcon(int iconResId) {
            params.iconResId = iconResId;
            return this;
        }

        /**
         * setTitle
         *
         * @param title  String
         * @return Builder Builder
         */
        public Builder setTitle(String title) {
            params.title = title;
            return this;
        }

        /**
         * setTitle
         *
         * @param resId int
         * @return Builder Builder
         */
        public Builder setTitle(int resId) {
            params.title = context.getString(resId);
            return this;
        }

        /**
         * setMessage
         *
         * @param message String
         * @return Builder Builder
         */
        public Builder setMessage(String message) {
            params.message = message;
            return this;
        }

        /**
         * setMessage
         *
         * @param resId int
         * @return Builder Builder
         */
        public Builder setMessage(int resId) {
            params.message = context.getString(resId);
            return this;
        }

        /**
         * setDuration
         *
         * @param duration long
         * @return Builder Builder
         */
        public Builder setDuration(long duration) {
            params.duration = duration;
            return this;
        }

        /**
         * setTitleColor
         *
         * @param titleColor int
         * @return Builder Builder
         */
        public Builder setTitleColor(int titleColor) {
            params.titleColor = titleColor;
            return this;
        }

        /**
         * setMessageColor
         *
         * @param messageColor int
         * @return Builder Builder
         */
        public Builder setMessageColor(int messageColor) {
            params.messageColor = messageColor;
            return this;
        }

        /**
         * setBackgroundColor
         *
         * @param backgroundColor int
         * @return Builder Builder
         */
        public Builder setBackgroundColor(int backgroundColor) {
            params.backgroundColor = backgroundColor;
            return this;
        }

        /**
         * setActionColor
         *
         * @param actionColor int
         * @return Builder Builder
         */
        public Builder setActionColor(int actionColor) {
            params.actionColor = actionColor;
            return this;
        }

        /**
         * setAction
         *
         * @param action String
         * @param onActionClickListener OnActionClickListener
         * @return Builder Builder
         */
        public Builder setAction(String action, OnActionClickListener onActionClickListener) {
            params.action = action;
            params.onActionClickListener = onActionClickListener;
            return this;
        }

        /**
         * setAction
         *
         * @param resId int
         * @param onActionClickListener OnActionClickListener
         * @return Builder Builder
         */
        public Builder setAction(int resId, OnActionClickListener onActionClickListener) {
            params.action = context.getString(resId);
            params.onActionClickListener = onActionClickListener;
            return this;
        }

        /**
         * setActionWithIcon
         *
         * @param resId int
         * @param onActionClickListener
         * @return Builder Builder
         */
        public Builder setActionWithIcon(int resId, OnActionClickListener onActionClickListener) {
            params.actionIcon = resId;
            params.onActionClickListener = onActionClickListener;
            return this;
        }

        /**
         * setLayoutGravity
         *
         * @param layoutGravity int
         * @return Builder Builder
         */
        public Builder setLayoutGravity(int layoutGravity) {
            params.layoutGravity = layoutGravity;
            return this;
        }

        /**
         * create
         *
         * @return CookieBar CookieBar
         */
        public CookieBar create() {
            CookieBar cookie = new CookieBar(decorView, params);
            return cookie;
        }

        /**
         * show
         *
         * @return CookieBar CookieBar
         */
        public CookieBar show() {
            final CookieBar cookie = create();
            cookie.show();
            return cookie;
        }
    }

    final static class Params {

        public String title;

        public String message;

        public String action;

        public OnActionClickListener onActionClickListener;

        public int iconResId;

        public int backgroundColor;

        public int titleColor;

        public int messageColor;

        public int actionColor;

        public long duration = 2000;

        public int layoutGravity = LayoutAlignment.TOP;

        public int actionIcon;
    }

}
