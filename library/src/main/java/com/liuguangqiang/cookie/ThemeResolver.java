package com.liuguangqiang.cookie;

import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Eric on 2017/3/21.
 */
public class ThemeResolver {

    /**
     * getColor
     *
     * @param color
     * @param defaultColor
     * @return Color
     */
    public static Color getColor(Color color, Color defaultColor) {
        if (color == null) {
            return defaultColor;
        }
        return color;
    }

    /**
     * getColor
     *
     * @param color
     * @param defaultColorId
     * @return Color
     */
    public static Color getColor(Color color, int defaultColorId) {
        if (color == null) {
            return new Color(defaultColorId);
        }
        return color;
    }

    /**
     * getColor
     *
     * @param context
     * @param colorId
     * @return int
     */
    protected static int getColor(Context context, int colorId) {
        int color = 0;
        try {
            color = context.getResourceManager().getElement(colorId).getColor();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.INFO, "IOException");
        } catch (NotExistException e) {
            Logger.getGlobal().log(Level.INFO, "NotExistException");
        } catch (WrongTypeException e) {
            Logger.getGlobal().log(Level.INFO, "WrongTypeException");
        }
        return color;
    }

    /**
     * getColor
     *
     * @param colorId colorId
     * @return int
     */
    protected static Color getColor(int colorId) {
        return new Color(colorId);
    }

}
